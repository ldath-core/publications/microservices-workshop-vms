#!/usr/bin/env bash
mkdir public
cp -R images public/
cp microservices-workshop-vms.html public/index.html
cp microservices-workshop-vms.pdf public/
